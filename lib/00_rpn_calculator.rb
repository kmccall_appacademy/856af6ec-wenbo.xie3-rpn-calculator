class RPNCalculator
  def initialize
    @stack = []
  end

  def value
    @stack.last
  end

  def push(num)
    @stack << num
  end

  def plus
    operation(:+)
  end

  def minus
    operation(:-)
  end

  def times
    operation(:*)
  end

  def divide
    operation(:/)
  end

  def tokens(string)
    operators = %w(+ - * /)

    string.split.map do |char|
      if operators.include?(char)
        char.to_sym
      else
        char.to_i
      end
    end
  end

  def evaluate(string)
    tokens(string).each do |el|
      if el.is_a?(Integer)
        push(el)
      else
        operation(el)
      end
    end

    value
  end

  def operation(op)
    raise "calculator is empty" if @stack.count < 2

    right_num = @stack.pop
    left_num = @stack.pop

    push([left_num.to_f, right_num.to_f].reduce(op))
  end
end
